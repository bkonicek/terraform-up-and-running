output "neo_arn" {
  description = "The ARN for Neo"
  value       = aws_iam_user.example["neo"].arn
}

output "all_arns" {
  description = "The ARN for all users"
  value       = values(aws_iam_user.example).*.arn
}

output "all_users" {
  description = "Map of all users"
  value       = aws_iam_user.example
}
