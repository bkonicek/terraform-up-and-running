# partial configuration - everything but 'key' moved to backend.hcl.
# add '-backend-config=backend.hcl' when running 'terraform init'
terraform {
  backend "s3" {
    bucket = "terraform-up-and-running-state-bk1226192"
    key    = "global/s3/terraform.tfstate"
    region = "us-east-1"

    dynamodb_table = "terraform-up-and-running-locks"
    encrypt        = true
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-up-and-running-state-bk1226192"

  #   Prevent accidental deletion of this bucket
  lifecycle {
    prevent_destroy = true
  }

  #   Enable versioning so we can see the full revision history of our state files
  versioning {
    enabled = true
  }

  # enable server-side encryption by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-up-and-running-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

