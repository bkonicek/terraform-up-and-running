#!/bin/bash

# Linux Academy sandboxes automatically are destroyed after
# a set amount of time. After restarting the sandbox this
# script will re-initialize an S3 bucket and DynamoDB for the remote state.
# First, run 'aws configure' to update the access keys

# cleanup old .terraform directories
find ../ -type d -name ".terraform" -exec rm -rf {} +

# Deploy default VPC, S3 bucket, and dynamodb
terraform init
terraform apply --auto-approve
# Copy state into ../global/s3 so it can be migrated to remote state
cp ./terraform.tfstate ../global/s3/terraform.tfstate
rm *terraform*

# Configure S3 backend, copying the state from the setup script into the new backend
cd ../global/s3
terraform init -force-copy
terraform apply --auto-approve