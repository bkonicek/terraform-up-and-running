terraform {
  backend "s3" {
    key = "prod/data-stores/mysql/terraform.tfstate"
  }
}

provider "aws" {
  region = "us-east-1"
}

module "mysql" {
  source = "../../../../modules/data-stores/mysql"
  # source = "git@gitlab.com:bkonicek/terraform-modules.git//data-stores/mysql?ref=v0.0.1"

  db_prefix   = "prod"
  db_password = var.db_password
}
