terraform {
  backend "s3" {
    key = "prod/services/webserver-cluster/terraform.tfstate"
  }
}

provider "aws" {
  region = "us-east-1"
}

module "webserver_cluster" {
  source = "../../../../modules/services/webserver-cluster"
  # source = "git@gitlab.com:bkonicek/terraform-modules.git//services/webserver-cluster?ref=v0.0.1"

  ami = "ami-04b9e92b5572fa0d1"

  cluster_name           = "webservers-prod"
  db_remote_state_bucket = "terraform-up-and-running-state-bk1226192"
  db_remote_state_key    = "prod/data-stores/mysql/terraform.tfstate"

  instance_type      = "t2.micro"
  min_size           = 2
  max_size           = 10
  enable_autoscaling = true

  custom_tags = {
    Owner      = "team-foo"
    DeployedBy = "terraform"
  }
}
