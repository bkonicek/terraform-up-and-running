terraform {
  backend "s3" {
    # bucket = "terraform-up-and-running-state-bk1226191"
    key = "stage/services/webserver-cluster/terraform.tfstate"
    # region = "us-east-1"

    # dynamodb_table = "terraform-up-and-running-locks"
    # encrypt        = true
  }
}

provider "aws" {
  region = "us-east-1"
}

module "webserver_cluster" {
  source = "../../../../modules/services/webserver-cluster"
  # source = "git@gitlab.com:bkonicek/terraform-modules.git//services/webserver-cluster?ref=v0.0.1"

  ami         = "ami-04b9e92b5572fa0d1"
  server_text = "New server text"

  cluster_name           = "webservers-staging"
  db_remote_state_bucket = "terraform-up-and-running-state-bk1226192"
  db_remote_state_key    = "stage/data-stores/mysql/terraform.tfstate"

  instance_type      = "t2.micro"
  min_size           = 2
  max_size           = 2
  enable_autoscaling = false
}

resource "aws_security_group_rule" "allow_testing_inbound" {
  type              = "ingress"
  security_group_id = module.webserver_cluster.alb_security_group_id

  to_port     = 12345
  from_port   = 12345
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}
